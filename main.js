const current = document.querySelector('#current');
const imgs = document.querySelectorAll('.imgs img');
const opacity = 0.6;

// default behaviour: select 1st image of the list
imgs[0].style.opacity = opacity;


imgs.forEach( (img, index) => img.addEventListener('click', (e) => {
    imgs.forEach( (img) => img.style.opacity = 1 );
    // change current image to src of clicked image
    current.src = e.target.src;
    // add fade-in class
    current.classList.add('fade-in');
    // remove fade-in class
    setTimeout( () => current.classList.remove('fade-in'), 500);
    e.target.style.opacity = opacity;
}));